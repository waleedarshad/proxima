class CreateProximaCharities < ActiveRecord::Migration[5.0]
  def change
    create_table :proxima_charities do |t|
      t.string :name
      t.string :description
      t.string :location
      t.string :cause
      t.integer :category_id
      t.string :cause
      t.string :address
      t.string :tel
      t.string :fax
      t.string :website_link
      t.integer :overall_rating

      t.timestamps
    end
    add_index :proxima_charities, :category_id
  end
end
