class AddWebLinkToProximaCharities < ActiveRecord::Migration[5.0]
  def change
    add_column :proxima_charities, :charity_nav_link, :string
  end
end
