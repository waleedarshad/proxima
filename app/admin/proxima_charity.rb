ActiveAdmin.register ProximaCharity , as: "Charity" do



permit_params :name,:description,:location,:cause,:cateogory_id,:cause,:address,:tel,:fax,:website_link,:overall_rating

index do 
	selectable_column 
	id_column
	column :name
	column :address
	column :location
	column :overall_rating
	column :created_at
	actions
end

end
