module Session
  extend ActiveSupport::Concern

  included do
    class_attribute :site_root
  end

  protected

  def site_root
    self.class.site_root
  end

  def session
    @session ||= create_session
  end

  def visit(url)
    visiting(url)

    if session.respond_to?(:visit)
      session.visit(url)
    else
      session.get(url)
    end
  end

  def visiting(url)
    print "Visiting url: '#{ url }'"
    # Zig.app.logger.debug "Visiting url: '#{ url }'"
  end

  def create_session
    Capybara::Session.new(:poltergeist)
  end

  def root_uri
    @root_uri ||= URI(site_root)
  end

  def root_url
    "#{ root_uri.scheme }://#{ root_uri.host}"
  end
end

module Proxima
  class Crawler

    # include Attempts
    include Session

    class_attribute :site_key

    def crawl
      begin
        process_pages
      rescue => exception
        @response = Response.new(results, true, debug_information_from_exception(exception), exception.is_a?(Capybara::Poltergeist::TimeoutError))
      else
        @response = Response.new(results)
      end

      @response
    end

    protected

    # def in_gallery_of_unknown_length(&block)
    #   reset_attempts!

    #   while another_attempt_allowed?
    #     break unless image && description

    #     if block.arity == 1
    #       block.call(attempts)
    #     else
    #       block.call
    #     end
    #   end
    # end

    def results
      @results ||= []
    end

    def add_result(&block)
      result = Result.new
      block.call(result)

      # Zig.app.logger.debug("\n-----------------------------")
      # Zig.app.logger.debug(JSON.pretty_generate(result.to_hash))
      # Zig.app.logger.debug("\n-----------------------------\n\n")

      # made_attempt!

      results << result
      result
    end

    def debug_information_from_exception(exception)
      debug_text = exception.message + "\n"
      debug_text << exception.backtrace.take(15).join("\n")
      debug_text
    end

    def pull_people(str)
      str.scan(/([A-Z][\w-]*(\s+[A-Z][\w-]*)+)/).map{|i| i.first}.to_sentence
    end
  end

  class Response
    attr_reader :failure, :message, :results, :timeout_error

    def initialize(results, failure = false, message = nil, timeout_error = false)
      @results = results.map(&:to_hash)
      @failure = failure
      @message = message
      @timeout_error =  timeout_error
    end

    def failure?
      @failure === true
    end
  end


  class Result
    attr_accessor :source, :title, :caption, :path, :people, :series_root, :series_item, :published_at
    attr_accessor :width, :height

    def title
      @title || ''
    end

    def caption
      @caption || ''
    end

    def to_hash
      {
        source: source,
        title: title,
        caption: caption,
        path: path,
        people: people,
        series_root: series_root,
        series_item: series_item,
        published_at: published_at,
        width: width,
        height: height
      }
    end
  end



end
