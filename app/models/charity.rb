require "byebug"
class Charity < Proxima::Crawler
 
  # self.site_key     = 'perezhilton.com'
  self.site_root    = 'https://www.charitynavigator.org/'
  # self.max_attempts = 10

  protected

  def process_pages
    
    pagniation_url.each do |url|
      
      current_page = 1
      begin
        visit("#{site_root}#{url}")  
      rescue Exception => e
        begin
          visit("#{site_root}#{url}")  
        rescue Exception => e
          visit("#{site_root}#{url}")    
        end
      end
      

      while current_page <= total_page 
        begin
          visit("#{site_root}#{url}")

          
          session.all("table.highlight-matches tbody tr").each do |element|

            charity_nav_link = element.first("td.highlight-matches h3 a")[:href]
            name = element.first("td.highlight-matches h3 a").try(:text)
            description = element.first("td.highlight-matches p.tagline ").try(:text)
            location = element.first("td.highlight-matches h3 a").try(:text)
            category_string = element.first("td.highlight-matches .category").try(:text)
            
            

            location = category_string.split(" CATEGORY: ")[0].sub("LOCATION: ", "") rescue ''
            category =  category_string.split("CATEGORY: ")[1].split(" : ")[0].sub(" CAUSE","") rescue ''
            cause = category_string.split(" CAUSE : ")[1] rescue ''
            
            category = Category.find_or_create_by(name: category)
            
            

            ProximaCharity.create(name: name, description: description, location: location, category_id: category.id,cause:cause, charity_nav_link: charity_nav_link)

          end
          
          detail_page_urls.each do |link|
            begin
              visit(link)
                if session.first("a.close-not-now")
                  session.first("a.close-not-now").trigger(:click)
                end
                
                address = session.first("#leftnavcontent .rating p").text.split("tel: ")[0] rescue ''
                tel = session.first("#leftnavcontent .rating p").text.split("tel: ")[1].split(" fax: ")[0] rescue ''
                fax = session.first("#leftnavcontent .rating p").text.split("tel: ")[1].split(" fax: ")[1].split(" EIN: ")[0] rescue ''
                ein = session.first("#leftnavcontent .rating p").text.split("tel: ")[1].split(" fax: ")[1].split(" EIN: ")[1] rescue ''
                charity_nav_link = session.current_url.sub("https","http")
                overall_rating = session.first(".shadedtable strong title").text == "three stars" ? 3 : 4
                website_link = session.first("#orgSiteLink")[:href]
                
                
                @charity = ProximaCharity.find_by(charity_nav_link: charity_nav_link)

                @charity.update_attributes!(address: address,tel: tel,fax: fax,overall_rating: overall_rating,website_link: website_link)
            rescue Exception => e
              print "\n\n============================================ \n\n"
              print "\n\nskipping request time out error mostly #{e} \n\n"
              print "\n\n=============================================\n\n"
            end
            
          end

        rescue Exception => e
          print "\n\n============================================ \n\n"
          print "\n\nskipping request time out error mostly #{e} \n\n"
          print "\n\n=============================================\n\n"
        end        
        current_page = current_page+20  
      end

    end
  end
    
  def total_page
    session.all(".pageNav tr td strong")[1].try(:text).to_i
  end

  def detail_page_urls
      session.all("table.highlight-matches tbody tr td.highlight-matches h3 a").map do |link|
       link[:href]
     end
  end

  def pagniation_url
    current_page = 1
    ["index.cfm?FromRec=#{current_page}&bay=search.results&overallrtg=3","index.cfm?FromRec=#{current_page}&bay=search.results&overallrtg=4"]
  end

end